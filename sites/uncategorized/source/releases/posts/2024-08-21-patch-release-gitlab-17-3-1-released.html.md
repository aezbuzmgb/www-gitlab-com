---
title: "GitLab Patch Release: 17.3.1, 17.2.4, 17.1.6"
categories: releases
author: Ameya Darshan
author_gitlab: ameyadarshan
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.3.1, 17.2.4, 17.1.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/08/21/patch-release-gitlab-17-3-1-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.3.1, 17.2.4, 17.1.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [The GitLab Web Interface Does Not Guarantee Information Integrity When Downloading Source Code from Releases.](#the-gitlab-web-interface-does-not-guarantee-information-integrity-when-downloading-source-code-from-releases) | Medium |
| [Denial of Service by importing maliciously crafted GitHub repository](#denial-of-service-by-importing-maliciously-crafted-github-repository) | Medium |
| [Prompt injection in "Resolve Vulnerabilty" results in arbitrary command execution in victim's pipeline](#prompt-injection-in-resolve-vulnerabilty-results-in-arbitrary-command-execution-in-victims-pipeline) | Medium |
| [An unauthorized user can perform certain actions through GraphQL after a group owner enables IP restrictions](#an-unauthorized-user-can-perform-certain-actions-through-graphql-after-a-group-owner-enables-ip-restrictions) | Medium |

### The GitLab Web Interface Does Not Guarantee Information Integrity When Downloading Source Code from Releases.

An issue was discovered in GitLab CE/EE affecting all versions starting from 8.2 prior to 17.1.6 starting from 17.2 prior to 17.2.4, and starting from 17.3 prior to 17.3.1, which allows an attacker to create a branch with the same name as a deleted tag.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:H/A:N), 5.7).
It is now mitigated in the latest release and is assigned [CVE-2024-6502](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6502).

Thanks [st4nly0n](https://hackerone.com/st4nly0n) for reporting this vulnerability through our HackerOne bug bounty program.


### Denial of Service by importing maliciously crafted GitHub repository

A Denial of Service (DoS) issue has been discovered in GitLab CE/EE affecting all versions prior to 17.1.6, 17.2 prior to 17.2.4, and 17.3 prior to 17.3.1. A denial of service could occur upon importing a maliciously crafted repository using the GitHub importer.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H), 6.5). 
It is now mitigated in the latest release and is assigned [CVE-2024-8041](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-8041).

Thanks [a92847865](https://hackerone.com/a92847865) for reporting this vulnerability through our HackerOne bug bounty program.


### Prompt injection in "Resolve Vulnerabilty" results in arbitrary command execution in victim's pipeline

An issue was discovered in GitLab EE affecting all versions starting from 17.0 prior to 17.1.6 starting from 17.2 prior to 17.2.4, and starting from 17.3 prior to 17.3.1, allows an attacker to execute arbitrary command in a victim's pipeline through prompt injection.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:H/I:H/A:N), 6.4).
It is now mitigated in the latest release and is assigned [CVE-2024-7110](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-7110).

This vulnerability has been discovered internally by GitLab team member [Dennis Appelt](hhttps://gitlab.com/dappelt).


### An unauthorized user can perform certain actions through GraphQL after a group owner enables IP restrictions

An issue has been discovered in GitLab EE affecting all versions starting from 12.5 before 17.1.6, all versions starting from 17.2 before 17.2.4, all versions starting from 17.3 before 17.3.1. Under certain conditions it may be possible to bypass the IP restriction for groups through GraphQL allowing unauthorized users to perform some actions at the group level.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-3127](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3127).

Thanks [0x777](https://hackerone.com/0x777) for reporting this vulnerability through our HackerOne bug bounty program.


### Mattermost Security Updates July 2, 2024

Mattermost has been updated to versions 9.9.0, which contains several patches and security fixes.



## Bug fixes


### 17.3.1

* [Fix timeout when checking group dependencies (17.3 backport)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162860)
* [Resolve "Background migrations removed issues" (backport to 17.3)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162869)
* [Backport to 17.3: Fixes Geo Replication Details incorrectly empty](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162798)
* [17.3 Backport vulnerability migration bugfix](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162829)
* [Add debian 10 (Buster) to deprecated OS list](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7852)
* [Raise default PostgreSQL shared buffers minimum to 256 MB](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7868)
* [Include language server version in code suggestions](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162931)
* [Turn NotFound from Gitaly into 404 for InfoRefs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162968)

### 17.2.4

* [Backport 17.2: Build assets image when running release environments](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161900)
* [Backport DORA DF score recalculation](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161129)
* [Backport 17.2 - Do not run release-environments on tagging](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162292)
* [Remove stong_memoization for cloud connector services](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162311)
* [Check if columns exist before running credit card hashing background migration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161773)
* [Merge branch 'jennykim/remove-release-environment-canonical-pipeline' into 'master'](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162491)
* [Fix empty dependency list page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162523)
* [Backport 17-2: handle empty repository.ff_merge](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162544)
* [17.2 backport for: Resolve "Background migrations removed in 17.1 cause upgrade issues"](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162868)
* [Include language server version in code suggestions](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162935)
* [Turn NotFound from Gitaly into 404 for InfoRefs)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162969)

### 17.1.6

* [Backport 17.1: Release Environments - pipeline level resource group](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161489)
* [Backport 17.1: Build assets image when running release environments](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161901)
* [Backport 17.1 - Do not run release-environments on tagging](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162293)
* [Fix backport gitlab-qa shm fix to 17.1 stable branch version](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162001)
* [Backport canonical RE downstream pipeline removal](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162492)
* [Update minimum Go version requirement for self-compiled (17.1)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162771)
* [Backport 17-1: handle empty repository.ff_merge](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162542)
* [Resolve "Background migrations removed issues" (backport to 17.1)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162865)
* [Fix: backport !157455 to 17-1-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/163169)
* [Include language server version in code suggestions](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/162936)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

Note: GitLab releases have skipped 17.2.3 and 17.1.5 . There are no patches with these version numbers.

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
