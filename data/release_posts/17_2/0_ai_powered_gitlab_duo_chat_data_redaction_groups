---
features:
  primary:
  - name: "Gitlab Duo disabling input and output logging by default."
    available_in: [free, premium, ultimate]
    add_ons: ["GitLab Duo Pro", "GitLab Duo Enterprise"]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#data-retention'
    image_url: '/images/17_0/gitlabduo.png'
    reporter: pwietchner
    stage: ai-powered
    stage_url: '/stages-devops-lifecycle/'
    categories:
    - GitLab Duo Chat
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/13401'
    description: |
      GitLab is now disabling AI input and output logging for GitLab Duo by default.

      At GitLab, we aim to ensure that customers have sovereignty over their data.
      We've now disabled input and output logging by default and will only log inputs and outputs with customers' explicit 
      consent via a GitLab Support ticket.
